# May 2023 Newsletter

* The next [CentOS Connect](https://connect.centos.org/) will be colocated at [Flock](https://flocktofedora.org/) on August 2. Watch the website or social media for CFP and registration details.

* The CentOS project has started a podcast called [Connections](https://www.youtube.com/playlist?list=PLuRtbOXpVDjCeyyTAJ23OsdjhPiIsKumg), where we talk to interesting people doing interesting things across the CentOS ecosystem. For our first episode, we talked to [Fedora Community Architect Justin Flory about Red Hat Summit, Fedora, and Flock](https://youtu.be/JaZAB2VwnzU).

* Read the important notice about [end dates for CentOS Stream 8 and CentOS Linux 7](https://blog.centos.org/2023/04/end-dates-are-coming-for-centos-stream-8-and-centos-linux-7/).

* The CentOS board has [approved the new ISA SIG](https://git.centos.org/centos/board/issue/115), which will explore enabling new processor features in CentOS builds. The team will focus on x86_64, but is open to contributors working on other architectures.

* The Cloud SIG has announced the availability of the [RDO build for OpenStack 2023.1 Antelope](https://lists.centos.org/pipermail/centos-devel/2023-April/142884.html) for CentOS Stream and related systems.

* Fedora ELN x86_64 builds are [switching to the x86_64-v3 baseline](https://lists.centos.org/pipermail/centos-devel/2023-March/142814.html). This will be the baseline for CentOS Stream 10 next year.

* Adam Samalik announced that [CentOS Stream 8 is now ahead of RHEL 8](https://lists.centos.org/pipermail/centos-devel/2023-March/142831.html), following the same workflow as Stream 9. This was the intended workflow, but there was transition time on the engineering processes in the 8 timeframe.


## SIG Reports

Each month, we publish a rotating selection of quarterly reports from our [Special Interest Groups](https://wiki.centos.org/SpecialInterestGroup/). This month includes reports from the Hyperscale, Kmods, and Cloud SIGs.


## [Hyperscale SIG](https://wiki.centos.org/SpecialInterestGroup/Hyperscale)

The Hyperscale SIG focuses on enabling CentOS Stream deployment on large-scale infrastructures and facilitating collaboration on packages and tooling.

Read the full [Hyperscale SIG report](https://blog.centos.org/2023/04/centos-hyperscale-sig-quarterly-report-for-2023q1/).

* All of our conference talks are collected [here](https://sigs.centos.org/hyperscale/communication/talks/). Here are the recent ones:
  * Davide Cavalca presented the [Hyperscale SIG update](https://connect.centos.org/#hyperscale) at CentOS Connect.
  * Michel Salim presented [One year on: Experiences using ebranch to bring over Fedora packages to EPEL](https://connect.centos.org/#ebranch) at CentOS Connect and [Upstream First: Meta's Linux Userspace, meet Linux Distributions](https://www.socallinuxexpo.org/scale/20x/presentations/upstream-first-metas-linux-userspace-meet-linux-distributions) at SCaLE 20x.
  * Alvaro Leiva Geisse and Anita Zhang hosted a [systemd workshop](https://www.socallinuxexpo.org/scale/20x/presentations/workshop-guided-journey-heart-systemd) at SCaLE 20x.

* The SIG gained 4 new members: Dalton Miner, Oleg Obleukhov, Vadim Fedorenko, and Alexander Bulimov.

* We hosted our second face to face Hypercale meetup at CentOS Connect/FOSDEM 2023. You can catch [the notes here](https://pagure.io/centos-sig-hyperscale/sig/blob/main/f/meetups/20230202-brussels-meetup.md).

* We've enhanced our systemd Hyperscale documentation with additional instructions for release testing and SELinux policy updates.

* Our container build pipeline is fully automated, and [container images](https://sigs.centos.org/hyperscale/content/containers/) are built on the CentOS OpenShift CI/CD infrastructure and published weekly on [Quay](https://quay.io/centoshyperscale/centos).

* We have worked with CPE to enable the ability to use [KIWI](https://osinside.github.io/kiwi/) to build operating system images through CBS.

* All packages coming from Hyperscale will have a “CentOS Hyperscale SIG” vendor tag. This both makes provenance clear, and allows one to set allow_vendor_change=False in DNF to avoid SIG packages being replaced by stock CentOS packages with higher NEVRAs.
 
* An optimized version of zlib is in our [“hyperscale-intel”](https://sigs.centos.org/hyperscale/content/repositories/intel/) repository. This version is identical to the one in CentOS Stream, but with two alternative hash implementations to provide fast and high-quality hash function on systems supporting SSE4.2 and AVX2 instructions.

* The latest kernel version in the Hyperscale SIG is kernel-5.14.0-76.hs2.hsx for CentOS Stream 8 and kernel-5.14.0-258.hs3.hsx.el9 for CentOS Stream 9.

* Package [systemd](https://github.com/systemd/systemd) has been updated to version 252.4.
* Package [squashfs-tools](https://github.com/plougher/squashfs-tools) has been updated to version 4.5.1.
* Package [valgrind](https://valgrind.org/) has been updated to version 3.20.0.
* Package [grep](https://www.gnu.org/software/grep/) has been updated to version 3.9.
* Package [libslirp](https://gitlab.freedesktop.org/slirp/libslirp) has been updated to version 4.4.0.


## [Kmods SIG](https://wiki.centos.org/SpecialInterestGroup/Kmods)

The Kmods SIG focuses on packaging and maintaining kernel modules for CentOS Stream and Enterprise Linux.

* No SIG members have been added since last report. We welcome anybody that’s interested and willing to do work within the scope of the SIG to join and contribute.

* A talk about the current status of the Kmods SIG and its used automation has been given at the [CentOS Connect](https://connect.centos.org/) in Brussels, Belgium. A recording is available on [YouTube](https://youtu.be/T59Mq2GToN4).

* Fixed automation for Stream 8. This issue was caused by the recent modification of the Stream 8 workflow.


## [Cloud SIG](https://wiki.centos.org/SpecialInterestGroup/Cloud)

Packaging and maintaining different FOSS based Private cloud infrastructure applications that one can install and run natively on CentOS Stream.

General:

* A new RPM containing the CentOS Cloud SIG RPM GPG pubkey has been created: centos-release-cloud. This package is required by the also newly created centos-release-okd RPM, and going forward will also be required by the centos-release-openstack RPM that has so far contained the key.

RDO:

* CloudSIG has published the RDO Antelope release on Apr 18th based on upstream OpenStack Antelope. ([https://lists.centos.org/pipermail/centos-devel/2023-April/142884.html](https://lists.centos.org/pipermail/centos-devel/2023-April/142884.html))

* RDO is currently investigating the use of RPMs from the RDO CloudSiG repos to deploy them utilizing OpenStack-Ansible. The reason for this effort is to compensate for the retirement of the TripleO project as a deployment tool.

OKD:

* CloudSIG has built and published the cri-o and openshift-clients RPMs on CBS. Currently the builds aren't automated, but they will be in the future using [packit.dev](http://packit.dev/).

* This is a big step towards producing CentOS Stream CoreOS (aka SCOS) with purely CentOS-sourced RPMs.

* Furthermore, new releases of OKD/SCOS were published, with the latest ones moving the stable release stream to version 4.13 and the .next release stream to version 4.14 ([https://github.com/okd-project/okd-scos/releases](https://github.com/okd-project/okd-scos/releases))

Documentation:

* The documentation of Cloud SIG is now available at [https://sigs.centos.org/cloud/](https://sigs.centos.org/cloud/)
* The mkdocs based repository contains the source and is populated on [https://pagure.io/centos-sig-cloud/documentation](https://pagure.io/centos-sig-cloud/documentation)
