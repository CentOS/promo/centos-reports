# February 2023 Newsletter

## CentOS Connect at FOSDEM

![CentOS Connect](https://connect.centos.org/connect-card-c10.png)

CentOS hosted its annual [CentOS Connect](https://connect.centos.org/) at FOSDEM 2023. CentOS Connect is a series of mini-conferences where people from across the Enterprise Linux ecosystem can connect at learn. CentOS Connect at FOSDEM is the largest of these events.

This was our first return to being in person at FOSDEM, and we were very happy with the turnout and overall experience. We also ran the event virtually. We're committed to running hybrid events, and we'll continue to improve on the virtual experience. If you attended, either in person or virtually, check your email for a post-event survey. It only takes a few minutes, and it really helps us create better events for you in the future.

If you couldn't make it, the [videos are available](https://www.youtube.com/playlist?list=PLuRtbOXpVDjCPdIyHl7saOLScz-hL0G_9) to watch on YouTube.


## Other FOSDEM Happenings

* CentOS had a booth at FOSDEM, right next to the Fedora booth. Our hats, mugs, and shirts were all gone by the end of the first day.

* We were in the [distributions devroom](https://fosdem.org/2023/schedule/track/distributions/). Adam Samalik presented about [CentOS Stream](https://fosdem.org/2023/schedule/event/centos_stream/).

* The CentOS Hyperscale SIG held a meetup the Thursday before CentOS Connect.

* We held a web+docs meetup the Monday after FOSDEM.


## New Social Media

We started official CentOS pages on [Facebook](https://www.facebook.com/CentOSProject) and [LinkedIn](https://www.linkedin.com/company/centos-project/). And in case you missed it, we're also on [Mastodon](https://fosstodon.org/@centos).


## KDE EPEL Updates

Troy Dawson [proposed changes to KDE packaging in EPEL](https://lists.centos.org/pipermail/centos-devel/2023-January/142743.html). First, KDE Plasma Desktop in EPEL 8 will now stay at the current release, receiving only major security fixes, and possibly bug fixes when possible. Second, in EPEL 9 and beyond, there will be updates every six months, instead of once a year, corresponding with each minor RHEL release.


## SIG Reports

Each month, we publish a rotating selection of quarterly reports from our [Special Interest Groups](https://wiki.centos.org/SpecialInterestGroup). This month includes reports from the Cloud, NFV, Storage, and Promo SIGs.


## [Cloud SIG](https://wiki.centos.org/SpecialInterestGroup/Cloud)

### Purpose

Packaging and maintaining different FOSS based Private cloud infrastructure applications that one can install and run natively on CentOS Stream.

### Future activities

The SIG will work on building and delivering the RPMs consumed by OKD on SCOS.

### Releases

There were no releases during this quarter. See below the most recent releases for each community:

* [RDO Zed release](https://lists.centos.org/pipermail/centos-devel/2022-November/120679.html)
* [MVP of OKD/SCOS](https://www.okd.io/blog/2022-10-25-OKD-Streams-Building-the-Next-Generation-of-OKD-together/)

### Documentation

The documentation of Cloud SIG is in the process of migration from the old CentOS wiki to the new [sigs.centos.org site](http://sigs.centos.org/). A new mkdocs based repository has been created and populated in [https://pagure.io/centos-sig-cloud/documentation](https://pagure.io/centos-sig-cloud/documentation).


## [NFV SIG](https://wiki.centos.org/SpecialInterestGroup/NFV)

### Purpose

The CentOS Network Functions Virtualization SIG provides a CentOS-based stack that will serve as a platform for the deployment and testing of virtual network functions (VNFs) and NFV component packages on CentOS platform.

Currently, the main goal is to provide RPM packages of [OpenvSwitch](https://www.openvswitch.org/) and [Open Virtual Network](https://www.ovn.org/en/) software for CentOS Stream that can be used by other projects as oVirt, OpenStack or OpenShift.

### Releases

New builds of openvswitch 2.15, 2.16 and 2.17 and ovn 2022 has been built and published in NFV OpenvSwitch repository.

### Architecture

OpenShift community has [shown interest in openvswitch for s390x architecture](https://lists.centos.org/pipermail/centos-devel/2023-January/142747.html).

Currently, CBS doesn't provide support to build s390x packages. As a workaround, builds of openvswitch for s390x have been created from latest NFV SIG srpms in Fedora COPR builder.

I wonder if there are any plan to enable this architecture in CBS and if other SIGs have received a similar request.

### Documentation

The documentation for NFV SIG is in the process of migration from the old CentOS wiki to the new [sigs.centos.org](http://sigs.centos.org/)) site. A new mkdocs based repository has been created and populated in [https://pagure.io/nfv-sig/](https://pagure.io/nfv-sig/).


## [Storage SIG](https://wiki.centos.org/SpecialInterestGroup/Storage)

* Glusterfs no updates to glusterfs
* Ceph Quincy (17) no updates to ceph-17.
* Ceph Pacific (16) updated to ceph-16.2.11. packages are available for Stream 9, Stream 8, and CentOS 8.
* NFS-ganesha 4 and libntirpc 4 updated to 4.3. packages are available for Stream 9, Stream 8, and CentOS 7. NFS-ganesha 5 and libntirpc 5 are coming soon.
* Apache Arrow no update to libarrow.
* Apache ORC no update to liborc.
* Samba 4.17, updated to latest version 4.17.5 on Stream 9 and Stream 8.
* Samba 4.16, updated to latest version 4.16.8 on Stream 9 and Stream 8.
* Samba 4.15, in CVE fixes only mode.


## [Promo SIG](https://wiki.centos.org/SpecialInterestGroup/Promo)

* Hosted [CentOS Connect](https://connect.centos.org) at FOSDEM.
* Hosted a booth at [FOSDEM](https://fosdem.org/2023/).
* Ran a web+docs meetups the day after FOSDEM. The team discussed the future of the wiki, consolidating various documentation resources, and general updates to our web sites.
* Hosted a booth at [OLF Conference](https://olfconference.org/).
