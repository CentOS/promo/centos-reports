# June 2024 News

* CentOS Linux 7 will be [EOL on June 30](https://blog.centos.org/2023/04/end-dates-are-coming-for-centos-stream-8-and-centos-linux-7/). Please migrate to CentOS Stream 9 or another suitable option.

* Various services that used [CentOS Linux 7 will be retired](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/thread/EGOA27SGN7W7ASFFFBNUZJ5BXSFT4NKX/) at the end of June. In particular, the CentOS Forums will be shut down, and redirect to the CentOS category on Fedora Discourse.

* The Alternative Images SIG has [released live images](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/thread/RM3JOXXCMLW2RBYOG2PJ3VXWZ2K2UTWX/) for multiple desktop environments.

* The CentOS mailing lists have been migrated to Mailman 3 with the [Hyperkitty web interface](https://lists.centos.org/hyperkitty/).

* CentOS [Stream 10 composes are available](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/thread/QCO73CKFMPNMERUWIQ47OVJMMUM7YUXU/) for testing and development. These are preview releases, and are not yet intended for production use.

* CentOS [Stream 10 targets are also now](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/thread/EW57G6NQEZEZIBBLS4FGHPSL44R6ZHI4/) available on the CentOS Community Build Service.

## [Kmods SIG](https://sigs.centos.org/kmods/)

The Kmods SIG focuses on packaging and maintaining kernel modules for CentOS Stream and Enterprise Linux.

* No SIG members have been added since last report. We welcome anybody that’s interested and willing to do work within the scope of the SIG to join and contribute.

* Thanks to good cooperation with Red Hat and help from the CentOS infrastructure team, the Kmods SIG is once again able to provide kernel modules built for RHEL targets.

* In addition to kernel modules built for Enterprise Linux kernels the Kmods SIG now also provides Fedora flavored kernels. This includes both the current stable release as well as LTS versions. See our [documentation on available packages](https://sigs.centos.org/kmods/packages/) for more details on provided kernel versions.

## [Virtualization SIG](https://sigs.centos.org/virt/)

The Virt-SIG aims to deliver a user-consumable full stack for virtualization technologies that want to work with the SIG. This includes delivery, deployment, management, update and patch application (for full lifecycle management) of the baseline platform when deployed in sync with a technology curated by the Virt-SIG..


* [Dorinda Bassey](https://fosdem.org/2024/schedule/speaker/C8BTZX/) presented at FOSDEM:
  - [Making VirtIO sing - implementing virtio-sound in rust-vmm project](https://fosdem.org/2024/schedule/event/fosdem-2024-1910-making-virtio-sing-implementing-virtio-sound-in-rust-vmm-project/)
  - [Pipewire audio backend in QEMU](https://fosdem.org/2024/schedule/event/fosdem-2024-1686-pipewire-audio-backend-in-qemu/)

* Seeteena dropped from Virtualization SIG as email address became invalid.

* We collaborated with CentOS Automotive SIG pushing upstream changes to rust-vmm related to virtio-sound and virtio-gpu.
  - oVirt released [oVirt Engine 4.5.6](https://github.com/oVirt/ovirt-engine/releases/tag/ovirt-engine-4.5.6) fixing a CVE.

## [ISA](https://sigs.centos.org/isa/)

The ISA SIG explores ways to deliver optimized package builds targeted at instruction set architecture (ISA) variants of architectures already supported by CentOS.

* The GNU2 TLS descriptors for x86-64 had ABI issues, which have been fixed in Fedora rawhide and Fedora 40. These changes are about to be integrated into CentOS 10 Stream.
* The x86 string function performance optimizations were merged from the Hyperscaler SIG repositories into mainline CentOS 8 Stream.
* Override packages with CPU optimizations that cannot be put into CentOS 9 Stream proper are now available and can be activated using `dnf install centos-release-isa-override` on CentOS 9 Stream.

* Phoronix used the ISA SIG builds for a benchmarking article: [CentOS Stream ISA Optimized Packages Show Great Results On Intel Xeon Emerald Rapids](https://www.phoronix.com/review/centos-isa-optimized)

* The current override packages are available here:

  - https://buildlogs.centos.org/9-stream/isa/x86_64/packages-override/Packages/i/

## [Alternative Images SIG](https://sigs.centos.org/altimages/)

The Alternate Images SIG's goal is to build and provide alternate iso images for CentOS Stream.

* Troy Dawson gave a presentation at CentOS Connect at FOSDOM.  It was titled [Alternative Images SIG - Let's Talk About It](https://www.youtube.com/watch?v=vwA4mULGiF8)

* We had our first quarterly update of [Live Images](https://mirror.stream.centos.org/SIGs/9-stream/altimages/images/live/)

* We have added four new [Live Images](https://mirror.stream.centos.org/SIGs/9-stream/altimages/images/live/).  We now have CINNAMON, GNOME, KDE, MATE, XFCE and MAX.  MAX has all of the other Live Image desktops, along with a few others.

* We have created scripts to make it easier to build images.

## [Artwork SIG](https://gitlab.com/CentOS/artwork)

The CentOS Artwork SIG exists to produce [The CentOS Project Visual Identity](https://wiki.centos.org/ArtWork/Identity).

* The jekyll-theme-centos theme pipeline is being rewritten based on [GitLab CI/CD Catalog](https://docs.gitlab.com/ee/architecture/blueprints/ci_pipeline_components/) concept.
* The jekyll-theme-centos theme pipeline is introducing HTML localization based on GNU Gettext approach. See [jekyll-site-l10n](https://gitlab.com/CentOS/artwork/centos-web/templates/jekyll-site-l10n), [pipeline example](https://gitlab.com/CentOS/artwork/centos-web/templates/jekyll-site-l10n/-/jobs/6409265601) and content translation test rendered from [English](https://centos.gitlab.io/artwork/centos-web/templates/jekyll-site-l10n/documentation/example/) to [Spanish](https://centos.gitlab.io/artwork/centos-web/templates/jekyll-site-l10n/es/documentation/example/).

## [Integration SIG](https://sigs.centos.org/integration/)

Provide a shared space to develop and maintain tooling and knowledge base on collaborative gating and testing of CentOS Stream updates before they are published to CentOS mirrors. This includes both - package-level and compose-level integration.

* An Integration SIG meeting was held at FOSDEM 2024
* Git Interface for the Compose Gate Proposal presented: [Issue](https://gitlab.com/groups/CentOS/Integration/-/epics/2), [Proposal](https://discussion.fedoraproject.org/t/centos-connect-follow-up-proposal-for-the-git-interface-to-the-compose-gate/104981)
* Continued with the migration of legacy tests to the new tmt+beakerlib format in the [compose-tests](https://gitlab.com/CentOS/Integration/compose-tests)
* Testing Farm integration completed for test contributions validations in the [compose-tests](https://gitlab.com/CentOS/Integration/)