# September 2024 News

* [Brian Exelbierd is stepping down](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/thread/QAG4NBSOEZFDLR7AWNZEE7TTIFL4YQIV/) as Red Hat Liaison to the CentOS Board of Directors. [Josh Boyer served](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/thread/BCWM3RZCYDMUOCGS6U5XH6W2EB55XR3U/) as an interim Liaison, leaving Josh's at-large seat on the Board vacant. [Nominations are open](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/thread/4R2KDJ55JQPFXJ5I7T7CULTLTS3SRMJ3/) for a new director.
* Subsequently, Josh announced that [Steve Wanless will be serving](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/thread/WG2SLMSEODL4ZJW2HZD2LLP3TWXAG3JR/) as the new Red Hat Liaison.
* The Cloud SIG is [taking nominations](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/thread/KOMS23S3B6WQYO3NV6CIOPIRVATUYN7T/) for a new chair.
* The Docs SIG held a [CentOS Doc Day](https://blog.centos.org/2024/09/centos-doc-day-at-flock/) at Flock.


## [Alternative Images SIG](https://sigs.centos.org/altimages/)

The Alternate Images SIG's goal is to build and provide alternate iso images for CentOS Stream.

**MIN**
* MIN is a new, text only Live image.  You automatically are logged in at the console.
* To do a text-only install run './install_to_hard_drive'

**Updated CentOS Logos**
* This isn't really anything our SIG did, but the logo's were updated in CentOS Stream 9, and this is the first release with the new CentOS logos.

**Bugs fixed**
* We were accidentally pulling in a few packages not in CentOS Stream, or EPEL.  This has been fixed.


## [Storage SIG](https://sigs.centos.org/storage/)

The CentOS Storage Special Interest Group (SIG) is a collection of like-minded individuals coming together to ensure that CentOS is a suitable platform for many different storage solutions. This group will ensure that all Open Source storage options seeking to utilize CentOS as a delivery platform have a voice in packaging, orchestration, deployment, and related work

### Stream 9 packages

- There have been no updates to Ceph Quincy (17). (Next update expected after the 19.2.0 release.
- Ceph Reef (18) has been updated to 18.2.4
- Ceph Squid (19) Release Candidate (RC) 19.1.1 has been released. It is only available in the testing repo.
- NFS-Ganesha 5 — including libntirpc 5 — has been updated to 5.9 and 5.8 respectively.
- NFS-Ganesha 6 — including libntirpc 6 — has been released. Both are currently at version 6.0. The centos-release-nfs-ganesha-6 package will be available by the time you read this.

### Stream 10 packages

Work is underway building the prerequisite packages in EPEL 10. Once they are available then Ceph Squid (19) and Ganesha 6 packages will be built.


## [Promo SIG](https://sigs.centos.org/promo/)

The Promo provides promotion and consistent messaging of the CentOS Project.

* The Promo SIG helped organize the CentOS track at [Flock 2024](https://fedoraproject.org/flock/2024/).
* While at Flock, we began work on our [Promo SIG documentation](https://sigs.centos.org/promo/).
* We have started planning work around CentOS Connect, our annual contributor conference that happens just before FOSDEM.
* We also started work on CentOS Showcase, a new series of virtual events. Stay tuned for an announcement soon!
* The Promo SIG has been working with the Artwork SIG on a revamp of the website, which we hope to deploy before the end of the year.
