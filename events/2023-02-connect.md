# CentOS Connect at FOSDEM 2023

This event report was written by Shaun McCance.

We held a [CentOS Connect](https://connect.centos.org/) on February 3, 2023 in Brussels as a fringe event for [FOSDEM](https://fosdem.org/2023/). This was our second in-person event since the pandemic, and we're continuing to learn how to run effective hybrid events.

## Venue

Our venue was the [DoubleTree Brussels City Center](https://www.hilton.com/en/hotels/brudtdi-doubletree-brussels-city/). We used the Pagoda room on the eighth floor. The venue worked very well for us. It was our understanding beforehand that we had a hard limit of 100 guests. In the past, Dojos at FOSDEM have had over 150 people, but we took the smaller space because in-person events have been slow to ramp back up. When we were setting up, we learned that the room was larger. We had it split in half with partition walls. The session room was able to accommodate 110 seated, and we had a social and registration space in addition to the smaller foyer where food was served. We allowed walk-ins and had over 120 people.

Given the interest and turnout, we should look to grow next year. We can probably fit about 150 in the Pagoda room, assuming that not everybody will sit for sessions at any given time. The hotel staff also gave us a tour of their ballroom. The ballroom can be split in half, with each half holding more than the Pagoda room. This could be used for two tracks, or for a very nice social space. Renting the ballroom also includes the hallway in front of it, where registration could be placed, as well as three small rooms that could be used for meetups or BOFs. This is probably more space than we need, and there can be a danger of an event feeling empty because it's in a space that's too large. Regardless, the DoubleTree was a great venue, and we should consider using it again if we can make the space work.


## Registration and Check-in

We used [Tito](https://tito.io/) for registration, both in-person and virtual. Tito has a number of nice features, including asking questions for things like shirt size and dietary needs, having different ticket types, sending messages to attendees by ticket type, and having check-in lists that don't require registration table workers to have an account. I've used Tito for a number of events in the past, and I'm very happy with it.

Check-in seemed to go smoothly. We had three people actively doing check-in in the morning when it's busiest, and two throughout the rest of the day. A single person could easily handle afternoon check-in. We had a separate ticket type for walk-ins, and asked people to register so we had their information. Unfortunately, I had set a limit on the number of walk-in tickets based on my assumptions about capacity, so later walk-ins weren't able to register. Registration staff did ask these people to write down their name and email address.

We had 100 registered attendees, and 91 check-ins. We had about 20 walk-ins attendees. We had communicated on social media that we were full, so we might have gotten more if we had encouraged it.

One issue with check-in was that we placed the table toward the back of the social area. This wasn't a problem in the morning, because people clearly saw the line of people checking in. But in the afternoon, it wasn't clear to people that there was a table where they should go. We should make the placement more prominent in the future, although swag storage put some limitations on us.

We printed badges with a blank space for people to write their names. This worked very well. Having names pre-printed requires lead time, and you always need blank ones anyway for late registrations and walk-ins. We also had a space on the badges for people to place one-inch stickers for project affiliation. We printed stickers for CentOS, Fedora, Red Hat, EPEL, RDO, and OKD. I'd like to do this again, but I'm considering making including a project in exchange for some level of promotion on social media to help us reach a broader audience.


## Virtual Event

This was our second time doing a hybrid event where we live streamed the presentations. We had feedback from the previous Dojo that we should have a hallway track for virtual attendees, so we set up a Google Meet for that. Unfortunately, the settings required people outside Red Hat to be explicitly let in, and I didn't have the bandwidth to watch the Meet and admit people. We should find a hallway track solution that's easy to use and just lets people in. However, we should also strongly consider having a trusted person in that track all day who can take care of issues (technical or otherwise) that might arise. This is something that could be done by a remote volunteer.

The live stream was done on YouTube, just as we did in the Dojo in Boston. YouTube is a popular choice because it has such a low barrier to entry for attendees. I watched the live stream for questions and relayed them to speakers. That way, virtual attendees were able to participate at least somewhat actively in the event.

At the previous Dojo, Boston University supplied us with a camera setup that let us switch between the camera and the slides. This was great, so I worked on getting the equipment to allow us to replicate that setup. For the most part, things worked well. There were some issues, which I'll describe in Technical Issues below.

We had asked people to register to attend online, even though the live stream was open to everybody. This let us get email addresses to send messages and the post-event survey, as well as collect information to send people swag. We periodically reminded people in the live stream chat to register. There were about 50 people registered for online, but we generally saw ten to fifteen online at any given time. I don't really have a good way of knowing who actually attended live. I'd like to figure out a better solution in the future.


## Sessions and Schedule

We had 13 talks, plus the Board AMA. We had a mix of talk lengths, offering speakers either short slots (20 minutes) or long slots (45 minutes). We did two talk lengths at the Boston Dojo as well, and it worked well. Some talks need more time, but most talks don't, and it keeps the event moving.

All of the long talks came in under the alotted time. We can consider shortening the long talk length in the future, but I'm worried about unnecessarily cutting some talks too short. However, all but two of the short talks run over. Three of them were nearly 30 minutes, four were around 25 minutes, and only two stayed inside their 20 minute slot.

We also had a very full schedule, with very little padding. Talks running over, coupled with some delays due to technical issues, meant that we were pushing into scheduled breaks. We did have five minutes of changeover time between talks. Changeovers were usually very fast, so that helped a little in making up time. Nonetheless, we should change the short talks to 25 minutes, and ideally have somebody to remind speakers of their time.

The scheduling problems also meant I didn't give speakers a proper intro. I also didn't have time to talk to speakers beforehand to get good info to make intros. I'd like to have time to emcee better in the future, or find a dedicated person to emcee instead.


## Swag

We ordered 150 shirts, 300 beanie hats, and 150 mugs. These were all ordered from a supplier in Brno. Since this event was colocated with FOSDEM, and we had a booth at FOSDEM, we planned to give out any extras at FOSDEM. We also had a shipment of shirts and backpacks with the old logo that we had to clear out. We controlled access to the new swag, but put the old swag on some tables for people to grab.

This worked out well in general, except we didn't have enough of the new shirts to cover shipping to virtual attendees. Because we allowed in more walk-ins than anticipated, our shirt stash was severely depleted. I ended up ordering another 150 shirts from a US vendor when I got home to send to virtual attendees. We did not send virtual attendees hats or mugs.

One thing I wanted to do was to send shirts to European virtual attendees before leaving, to save on shipping time and costs. This didn't work out, in part because of how low we were on shirts, but also because it takes time to get stuff together for shipping. To do this in the future, we should ensure we have enough shirts, prepare as much of the virtual swag packs beforehand as we can, and plan for either a post-even wrap day or have a European colleague handle the shipments.


## Technical Setup and Issues

At the Boston Dojo, we had a very nice setup provided by Boston University to deliver a live stream, so we tried to recreate that setup. The setup allowed us to switch between a camera feed and the speakers' slides for videos, and to deliver pre-mixed audio regardless of the video feed.

We had the venue provide just an audio setup: speakers, a few microphones, a line out jack for computer audio, and a mixing board. The mixing board had an aux out to feed into our setup. We provided a camera with HDMI output, an HDMI splitter to split speakers' slides between the projector and our feed, and an HDMI switching device with an audio line in. The HDMI switch connects to a computer with USB as if it were a webcam. We also provided various cables, in particular a 75' HDMI cable with a built-in repeater. We also provided a computer to receive the feed and do the streaming. We then streamed over YouTube. We could switch the HDMI input, and it was seamless to the computer.

This setup worked very well, but we did have various technical issues:

* We ended up having to use my laptop to stream instead of the computer we had intended. I had set up the YouTube live stream link from my laptop, and couldn't figure out how to make another device join that stream as the presenter, even though it was logged into the same account. Joining YouTube as the presenter was a frequent problem that came up again later.

* In our testing the day before, we were not getting an audio feed to the live stream. Tuomas and I were up late trying to solve this. Eventually we went to bed, and I emailed the venue asking to meet with their AV team in the morning. We did solve it in the morning. Their mixer used software on a tablet, instead of physical sliders, and I didn't have access to that the night before. I'm honestly not sure if that was the issue. In the future, we should try to do setup with the AV team the day before. We should also bring wired headphones to plug into the jack on the HDMI switch for testing and monitoring.

* Apple devices (Macs and iPads) didn't work. Their video would only go to the projector, and we got no video to our HDMI switch. This baffled me at the time. Further research indicates it's because of HDCP. HDCP allows an HDMI source device to encrypt data using a key negotiated with a display device. This is usually used for things with strong copy protection, like movies, but Apple devices just always use HDCP when available. I don't have a solution for this yet, but it seems there might be HDMI splitters that overcome HDCP.

* We lost our connection to YouTube twice. When this happened, we weren't able to rejoin as the presenting device, which means we had to create a new live stream and try to contact attendees. That was really bad. I had done some test streams before coming, but I never tested disconnecting myself. I need to play with YouTube streaming more to figure out if there's a way to rejoin as the presenter. Ideally, we shouldn't have connection issues, but if they happen, we need to be able to recover quickly.

* On that subject, the connection issues were probably caused by flaky hotel wifi. After the second disconnection, we managed to find an ethernet drop, and we didn't have any more problems. We also had issues with the wifi when a speaker tried to connect remotely. In the future, we should make sure we have ethernet for both the streaming device and the speaker.

* Also on that subject, we should look into whether we can record locally while also streaming. That way, even if we have an issue with the live stream, we still have good recordings. One option is to use software like OBS to manage to video feed. Alternatively, the HDMI switch also has an HDMI output, intended for preview. We may be able to use that with an HDMI capture device.


## Other Things to Improve

In addition to the other gear additions, we should add a longer (10' or so) HDMI cable to connect the splitter to the projecter input, to give us more flexibility in placement. Also, an HDMI extension between the camera and HDMI switch would give us more flexibility in camera placement. And we should add a US power strip or two for our equipment so that we don't have to find multiple outlets and plug multiple bulky adapters into them.

We should consider paying the venue for a camera setup. As long as their camera can provide an HDMI stream, we can use it with our setup. Packing a camera and (especially) a tripod is bulky and heavy. I ended up using a less nice tripod just because of packing constraints. Renting a camera setup would be an added expense, and it might not fit in the budget, but it's something worth looking at.

It was very difficult to keep up with social media. I wanted to make a post just before each talk to draw people to the live stream. Ideally, it would have a speaker photo, or perhaps the bumper image we put on the video recordings. The only machine I had logged in for our accounts was the streaming computer. It would be nice to have a dedicated person with their own device, and possibly having some posts ready to go beforehand.

When we went on break, especially to lunch, the live stream was left with a boring view of a room. At lunch, Tuomas had the idea to create a quick slide saying we were on lunch and feed it into the HDMI switch. We could have slides like this already prepared in the future, and add in information on joining the virtual hallway track. Tuomas also pointed out we could add an intro slide for each talk, using the same image we use as a bumper in the recordings we post later. Not only would this give us a nicer image during changeover, but it would also put the intro image already in the video for when we create the individual recording videos afterwards.
